# flask-demoapp

flask-demoapp is a minimal Python Flask "Hello World" application according to [Quickstart > A Minimal Application](https://flask.palletsprojects.com/en/2.2.x/quickstart/#a-minimal-application). We have built this to demonstrate how to run a Python project in the following setup:

- Python modules are installed in virtual environent `venv`
- Using `pip` as package manager
- Using **Systemd user unit** to start uWSGI
- Using **[uWSGI](https://uwsgi-docs.readthedocs.io/en/latest/)** as Python application server
- Using an **Apache / Nginx reverse proxy** to publish the app

This setup is optimized for limePRO customers hosted by [Onlime GmbH](https://www.onlime.ch/) - a Swiss hosting provider that cares about developer happiness!

Copyright (C) 2022 Onlime GmbH <https://www.onlime.ch>

## Install Project

Clone this repo:

```bash
$ git clone https://gitlab.com/onlime/flask-demoapp.git
```

Install and activate the venv, then install required Python modules:

```bash
$ cd flask-demoapp
$ python3 -m venv venv
$ . venv/bin/activate
(venv)$ pip install --upgrade pip
(venv)$ pip install -r requirements.txt
(venv)$ deactivate
```

Running the app locally:

```bash
$ . venv/bin/activate
(venv)$ flask run
 * Debug mode: off
 * Running on http://127.0.0.1:5000
```

then, check http://localhost:5000/ in your browser for the `Hello, World!` greeting.

```bash
$ curl http://localhost:5000
Hello, World!
```

## Setup Systemd user unit

### Setup Systemd service

Run below commands as regular system user, e.g. `web999`:

```bash
$ ssh web999@web.onlime.ch
```

Create systemd unit file in `~/.config/systemd/user/flask-demoapp.service`:

```bash
$ mkdir -p ~/.config/systemd/user
$ touch ~/.config/systemd/user/flask-demoapp.service
```

Edit `flask-demoapp.service` and make sure you use the absolut path to your project directory:

```ini
[Unit]
Description=uWSGI instance to serve flask-demoapp
AssertPathExists=/var/www/%u/public_html/flask-demoapp

[Service]
Type=simple
StandardOutput=journal
WorkingDirectory=/var/www/%u/public_html/flask-demoapp
ExecStart=/var/www/%u/public_html/flask-demoapp/venv/bin/uwsgi --yaml uwsgi.yml --socket /var/www/%u/tmp/%N.sock --chmod
Restart=on-failure
NoNewPrivileges=true

[Install]
WantedBy=default.target
```

Enable the service so it starts automatically and start it:

```bash
$ systemctl --user enable flask-demoapp.service
$ systemctl --user start flask-demoapp.service
```

Check if it's running:

```bash
$ systemctl --user status flask-demoapp.service
```

If you have changed anything in `flask-demoapp.service`, run:

```bash
$ systemctl --user daemon-reload
$ systemctl --user restart flask-demoapp.service
```

> **uWSGI as http server** (for testing)
>
> Instead of running your app on a Unix socket (recommended for production), you could instead run uWSGI as a http server. Copy `uwsgi-http-instance.yml_example` to `uwsgi-instance.yml` and configure your Systemd service like this:
>
> ```ini
> ExecStart=/var/www/%u/public_html/flask-demoapp/venv/bin/uwsgi --yaml uwsgi.yml --yaml uwsgi-instance.yml
> ```
>
> After having restarted the service, you could test the application with `curl`:
>
> ```bash
> $ curl http://localhost:8080/
> Hello, World!
> ```

### Automatic start-up

Above systemd user instance is started after the first login of the user and killed after the last session of the user is closed. We want this service to start right after boot, and keep the systemd user instance running after the last session closes.

As **root**, give the user permission to run services when they're not logged in:

```bash
$ sudo loginctl enable-linger web999
```

> You can show a list of lingering users with:
>
> ```bash
> $ ls /var/lib/systemd/linger
> ```

## Setup Reverse Proxy

At [Onlime GmbH](https://www.onlime.ch/), for our customers, we use Apache with [mod_proxy_uwsgi](https://httpd.apache.org/docs/trunk/mod/mod_proxy_uwsgi.html) as reverse proxy. The module is enabled as follows:

```bash
$ a2enmod proxy
$ a2enmod proxy_uwsgi
$ systemctl restart apache2
```

Our ProxyPass configuration looks like this:

```apache
<VirtualHost *:443>
  ServerName flask-demoapp.example.com
  # ...
  ProxyPass / unix:/var/www/web999/tmp/flask-demoapp.sock|uwsgi://localhost/
</VirtualHost>
```

## Other Runmodes

For security reasons, at [Onlime Webhosting](https://www.onlime.ch/) we recommend to only use Unix file sockets. If you run this Flask demoapp or any other Python project on a different hoster / dedicated server, you might also be interested in other runmodes via TCP ports.

Copy `uwsgi-http-instance.yml_example` to `uwsgi-instance.yml` and run uWSGI like this:

```bash
uwsgi --yaml uwsgi.yml --yaml uwsgi-instance.yml
```

We're going to present 4 different reverse proxy setups while preferring Variant 1 for both Apache or Nginx using **TCP sockets**. All setups should be robust.

### Apache

#### Variant 1 - proxy_uwsgi (recommended)

[Apache Module mod_proxy_uwsgi](https://httpd.apache.org/docs/trunk/mod/mod_proxy_uwsgi.html)

Use the following `uwsgi-instance.yml` config in your project:

```yaml
uwsgi:
  socket: 127.0.0.1:8080
```

If you haven't done so, enable Apache `proxy_uwsgi` module:

```bash
$ a2enmod proxy
$ a2enmod proxy_uwsgi
$ systemctl restart apache2
```

Then configure your VirtualHost as follows:

```apache
<VirtualHost *:443>
  ServerName flask-demoapp.example.com
  # ...
  ProxyPass / uwsgi://localhost:8080/
</VirtualHost>
```

#### Variant 2 - mod_rewrite

[Using mod_rewrite for Proxying](https://httpd.apache.org/docs/2.4/rewrite/proxy.html)

Use the following `uwsgi-instance.yml` config in your project:

```yaml
uwsgi:
  http: 127.0.0.1:8080
```

If you haven't done so, enable Apache `rewrite` module:

```bash
$ a2enmod rewrite
$ systemctl restart apache2
```

For Apache, use `mod_rewrite` for proxying with `RewriteRule`'s `[P]` flag:

```apache
<VirtualHost *:443>
  ServerName flask-demoapp.example.com
  # ...
  RewriteEngine On
  RewriteRule ^/(.*) http://localhost:8080/$1 [L,P]
</VirtualHost>
```

### Nginx

#### Variant 1 - Native uwsgi support (recommended)

[Nginx native uwsgi protocol support](https://uwsgi-docs.readthedocs.io/en/latest/Nginx.html)

Use the following `uwsgi-instance.yml` config in your project:

```yaml
uwsgi:
  socket: 127.0.0.1:8080
```

On Nginx, you can use the native uwsgi protocol support for best performance:

```nginx
server {
    listen          443;
    server_name     flask-demoapp.example.com;
    # ...
    location / {
        include uwsgi_params;
        uwsgi_pass 127.0.0.1:8080;
    }
}
```

#### Variant 2 - Reverse proxy

[NGINX Reverse Proxy](https://docs.nginx.com/nginx/admin-guide/web-server/reverse-proxy/)

Use the following `uwsgi-instance.yml` config in your project:

```yaml
uwsgi:
  http: 127.0.0.1:8080
```

To pass requests to the HTTP proxied server, the `proxy_pass` directive is specified inside a `location`:

```nginx
server {
    listen          443;
    server_name     flask-demoapp.example.com;
    # ...
    location / {
        proxy_pass http://127.0.0.1:8080;
    }
}
```

